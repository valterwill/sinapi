"""Módulo que contém as tarefas locais utilizadas com invoke."""

from invoke import *


@task
def makemigrations(settings='development'):
    """Gera os arquivos de migração."""
    cmd = './manage.py makemigrations --settings=sinapi.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)


@task
def migrate(settings='development'):
    """Aplica as migrações."""
    cmd = './manage.py migrate --settings=sinapi.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)


@task
def test(settings='test'):
    """Testa as aplicações do projeto (com exceção dos testes funcionais)."""
    cmd = 'coverage run ./manage.py test --settings=sinapi.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)
    cmd = 'coverage report'
    run(cmd, echo=True, pty=True)


@task
def functional_tests(package='functional_tests.histories', settings='test'):
    """Executa os testes funcionais."""
    collectstatic(settings, True)
    cmd = 'coverage run ./manage.py test {} . --settings=sinapi.settings.{}'
    cmd = cmd.format(package, settings)
    run(cmd, echo=True, pty=True)
    cmd = 'coverage report'
    run(cmd, echo=True, pty=True)


@task(default=True)
def run_server(settings='development'):
    """Executa o servidor web de desenvolvimento local."""
    cmd = './manage.py runserver --settings=sinapi.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)


@task
def collectstatic(settings='development', noinput=False, clear=False):
    """Coleta arquivos estáticos."""
    noinput = '--noinput' if noinput else ''
    clear = '--clear' if clear else ''
    cmd = './manage.py collectstatic {} {} --settings=sinapi.settings.{}'
    cmd = cmd.format(noinput, clear, settings)
    run(cmd, echo=True, pty=True)


@task
def install(settings='development'):
    """
    Instala migrações e arquivos estáticos.

    Essa tarefa é utilizada no ambiente de produção para configurar a aplicação:
    invoke install --settings production
    """
    run('pip3 install -r requirements.txt')
    makemigrations(settings)
    migrate(settings)
    run('npm install', echo=True, pty=True)
    run('bower install', echo=True, pty=True)
    collectstatic(settings, True)


@task
def celery(settings='development'):
    """Executa celery."""
    cmd = 'DJANGO_SETTINGS_MODULE=sinapi.settings.{} '
    cmd += 'celery -A sinapi worker -E -l info'
    cmd = cmd.format(settings)
    run(cmd, echo=True, pty=True)


@task
def send_queued_mail(settings='development'):
    """Envia emails enfileirados pela aplicação django_post_office."""
    cmd = './manage.py send_queued_mail --settings=sinapi.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)
