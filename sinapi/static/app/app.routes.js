(function () {
  'use strict';

  var app = angular.module('sinapi');
  app.config(routeConfig);
  app.run(function($rootScope, $state) {
    $rootScope.$state = $state;
  });

  function routeConfig($stateProvider, $urlRouterProvider, insumSearchURL, compositionSearchURL) {
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('index', {
      url: '/',
      template: '<h4>Utilize o menu lateral esquerdo para a consulta de tabelas.</h4>'
    });

    $stateProvider.state('record', {
      url: '/registro',
      abstract: true,
      template: '<div ui-view></div>'
    }).state('record.insum', {
      url: '/insumos',
      templateUrl: insumSearchURL,
      controller: 'insumCtrl',
      controllerAs: 'vm'
    }).state('record.composition', {
      url: '/composições',
      templateUrl: compositionSearchURL,
      controller: 'compositionCtrl',
      controllerAs: 'vm'
    });
  }
})();