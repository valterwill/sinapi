(function () {
  'use strict';

  angular.module('sinapi')
    .controller('insumCtrl', insumCtrl);

  function insumCtrl(RecordResource) {
    var self = this;
    self.excelQuery = '';
    self.searchInsum = _.debounce(searchInsum, 500);
    self.init = init;
    self.exportToExcel = exportToExcel;

    self.init();
    
    self.gridOptions = {
      columnDefs: [
        {name: 'Código', field: 'code'},
        {name: 'Descrição', field: 'description'},
        {name: 'Unidade', field: 'unity'},
        {name: 'Origem', field: 'origin'},
        {name: 'Preço', field: 'price', cellTemplate: 'insumPrice.html'}
      ],
      data:[]
    }

    function searchInsum() {
      self.searchInsumPromise = RecordResource.query({search: self.searchQuery, type: 'insum', month: 10, year: 2015}).$promise;
      self.searchInsumPromise.then(function (insums) {
        self.gridOptions.data = insums;
      });
    }

    function exportToExcel() {
      var codes = [];
      self.gridOptions.data.forEach(function (record) {
        codes.push(record.code);
      });
      self.excelQuery = codes.join(',');
    }

    function init() {
      document.getElementById('searchInsum').focus();
    }
  }
})();