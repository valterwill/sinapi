(function () {
  'use strict';

  angular.module('sinapi')
    .controller('compositionCtrl', compositionCtrl);

  function compositionCtrl(RecordResource) {
    var self = this;
    self.excelQuery = '';
    self.searchComposition = _.debounce(searchComposition, 500);
    self.init = init;
    self.exportToExcel = exportToExcel;

    self.init();
    
    self.gridOptions = {
      columnDefs: [
        {name: 'Código', field: 'code'},
        {name: 'Descrição', field: 'description'},
        {name: 'Unidade', field: 'unity'},
        {name: 'Origem', field: 'origin'},
        {name: 'Preço', field: 'price', cellTemplate: 'compositionPrice.html'}
      ],
      data:[]
    }

    function searchComposition() {
      self.searchCompositionPromise = RecordResource.query({search: self.searchQuery, type: 'composition', month: 10, year: 2015}).$promise;
      self.searchCompositionPromise.then(function (insums) {
        self.gridOptions.data = insums;
      });
    }

    function exportToExcel() {
      var codes = [];
      self.gridOptions.data.forEach(function (record) {
        codes.push(record.code);
      });
      self.excelQuery = codes.join(',');
    }

    function init() {
      document.getElementById('searchComposition').focus();
    }
  }
})();