"""Views da aplicação registros."""

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import View
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from .forms import ExportToExcelForm
from .models import Record
from .serializers import RecordSerializer


class InsumSearchView(APIView):

    """Página de consulta de insumos."""

    renderer_classes = [TemplateHTMLRenderer]
    permission_classes = [IsAuthenticated]
    template_name = 'records/insum.html'

    def get(self, request):
        """Obtém a página."""
        return Response()


class CompositionSearchView(InsumSearchView):

    """Página para consulta de composições."""

    template_name = 'records/composition.html'


class RecordViewSet(ListModelMixin, RetrieveModelMixin, GenericViewSet):

    """Permite listar e visualizar registros via api."""

    serializer_class = RecordSerializer
    queryset = Record.objects.all()
    permission_classes = [IsAuthenticated]
    search_fields = ('code', 'description', 'unity', 'origin', 'price')
    filter_fields = ('type', 'month', 'year')


class ExportToExcelView(LoginRequiredMixin, View):

    """Gera arquivos excel a partir de registros do banco de dados."""

    def post(self, request, *args, **kwargs):
        """Envia os bytes do arquivo excel para o cliente."""
        form = ExportToExcelForm(request.POST)
        if form.is_valid():
            return form.save()
