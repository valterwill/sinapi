"""Aplication config."""

from django.apps import AppConfig


class RecordsConfig(AppConfig):

    """Configuração da aplicação registros."""

    name = 'records'
