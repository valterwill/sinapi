"""Registros SINAPI."""

from django.db.models import CharField, TextField, DecimalField, IntegerField
from model_utils.models import TimeStampedModel

RECORD_CHOICES = [('insum', 'Insumo'), ('composition', 'Composição')]


class Record(TimeStampedModel):

    """Um registro no SINAPI podendo ser classificado como insumo ou composição."""

    code = CharField(max_length=15, primary_key=True)
    description = TextField()
    unity = CharField(max_length=15)
    origin = CharField(max_length=15)
    price = DecimalField(decimal_places=2, max_digits=20)
    month = IntegerField()
    year = IntegerField()
    type = CharField(max_length=15, choices=RECORD_CHOICES)

    def __str__(self):
        """toString."""
        return '[code={}][description={}][unity={}][origin={}][price={}][date={}/{}]'.format(
            self.code,
            self.description,
            self.unity,
            self.origin,
            self.price,
            self.month,
            self.year,
        )

    class Meta:
        ordering = ('code',)
