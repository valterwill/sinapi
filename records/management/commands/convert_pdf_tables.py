"""Converte uma tabela SINAPI no formato pdf em registros do banco de dados."""

from django.core.management import BaseCommand
from os.path import expanduser

from ...pdf2db.converter import CompositionParser, InsumParser, save_pdf_to_db


class Command(BaseCommand):

    """Executa a conversão PDF -> DB."""

    def add_arguments(self, parser):
        """Adiciona opções da linha de comando."""
        parser.add_argument('pdf_file', help='Caminho do arquivo pdf de entrada')
        parser.add_argument('parser', help='Tipo de parser ("insum" para insumos, "comp" para composições)')
        parser.add_argument('month', help='Mês referente ao lançamento da planilha.')
        parser.add_argument('year', help='Ano referente ao lançamento da planilha.')

    def handle(self, *args, **options):
        """Executa o comando."""
        if options['parser'] == 'insum':
            _parser = InsumParser

        elif options['parser'] == 'comp':
            _parser = CompositionParser

        else:
            raise TypeError('Opção inválida de parser.')

        save_pdf_to_db(expanduser(options['pdf_file']), _parser(options['month'], options['year']))
